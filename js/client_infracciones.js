/* Se crea una clase infraccion en la cual meter los datos de la infraccion de la api 
para la patente solicitada */
class Infraccion {
    constructor(id, patente, fechaHoraRegistro, fechaHoraActualizacion,
        direccionRegistrada, tipoInfraccion, montoAPagar, existeAcarreo, deposito) {
        this.id = id;
        this.patente = patente;
        this.fechaHoraRegistro = fechaHoraRegistro;
        this.fechaHoraActualizacion = fechaHoraActualizacion;
        this.direccionRegistrada = direccionRegistrada;
        this.tipoInfraccion = tipoInfraccion;
        this.montoAPagar = montoAPagar;
        this.existeAcarreo = existeAcarreo;
        this.deposito = deposito;
    }
}

/* Se crea una clase deposito en la cual meter los datos del deposito asociado a la 
infraccion en caso de que exista acarreo */
class Deposito {
    constructor(id, nombre, direccion, telefono, horarios, ubicacion) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.horarios = horarios;
        this.ubicacion = ubicacion;
    }
}

/* Se crea una clase ubicacion en la cual meter la ubicacion del deposito si es que
existe acarreo */
class Ubicacion {
    constructor(lat, lon) {
        this.lat = lat;
        this.lon = lon;
    }
}

/* Se agarran los datos de la patente que se le pasa con el metodo GET por URL */
const queryString = window.location.search;

const urlParams = new URLSearchParams(queryString);

const patente = urlParams.get('patente');

/* Se crean los datos de la url para hacer las consultas */
const url = "https://infraccionesweb.herokuapp.com/api/";

const urlInfracciones = "/infracciones/";

const urlTiposInfraccion = "/tiposInfraccion/";

const urlAcarreos = "/acarreos/";

/* Se crean las variables en las cuales inyectar codigo html */
const infraccionesLabel = document.getElementById("infracciones");
const infraccionesCheck = document.getElementById("existenInfracciones");

/* Funcion para darle otro formato a la fecha */
function parseFecha(date) {
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() +
        " a las " + date.getHours() + ":" + date.getMinutes() + "hs.";
}

/* Funcion para imprimir la ubicacion en el mapita */
function printLocation() {
   
    var depositoUbicacion = [localStorage.getItem("UBICACIONDEPOSITOLAT")
    , localStorage.getItem("UBICACIONDEPOSITOLON")];
	var depositoDireccion = localStorage.getItem("DIRECCIONDEPOSITO");

    var map = L.map('mapDepositoid').setView(depositoUbicacion, 20);
    

    var depositoMarker = L.marker(depositoUbicacion).bindPopup(depositoDireccion);
    depositoMarker.addTo(map);
	popup = L.popup().setLatLng(depositoUbicacion).setContent(depositoDireccion).openOn(map);
	
	
    
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
 
    document.getElementById("mapDepositoid").style.height = "300px";
    location.href='#mapa';
}

/* Funcion para consultar los datos de las infracciones */
async function fetchData(patente) {

    /* Se hace la consulta a la api con los datos de la patente */
    const res1 = await fetch(url + patente + urlInfracciones)
    const data = await res1.json();

    /* Si la llamada a la api con la patente no devuelve nada */
    if (data.infracciones.length === 0) {
        infraccionesCheck.innerHTML = "No existen infracciones para el vehículo con la patente " + patente +
            "<br><p><a href='infracciones.html'>" + "<input class='btn btn-primary' type='button' value='Volver' id='volver'></input></p>";
    }

    /* Si devuelve algo */
    else {

        infraccionesCheck.innerHTML = "Las infracciones para el vehículo con la patente " + patente + " son \n";

        var infracciones = new Array(data.infracciones.length);

        for (let i = 0; i < data.infracciones.length; i++) {

            /* Se realiza una consulta para obtener el tipo de infracción */
            res2 = await fetch(url + urlTiposInfraccion + data.infracciones[i].tipoInfraccion);
            tInfraccion = await res2.json();

            /* Si no existe acarreo, el deposito es null */
            deposito = null;

            /* Si existe acarreo, se realiza una consulta para obtener la informacion 
            del deposito y se crea el objeto con los datos del deposito */
            if (data.infracciones[i].existeAcarreo == true) {
                res3 = await fetch(url + patente + urlAcarreos + data.infracciones[i].id);
                dep = await res3.json();

                ubicacionDeposito = new Ubicacion(
                    dep.acarreo.deposito.ubicacion.lat,
                    dep.acarreo.deposito.ubicacion.lon);

                deposito = new Deposito(
                    dep.acarreo.deposito.id,
                    dep.acarreo.deposito.nombre,
                    dep.acarreo.deposito.direccion,
                    dep.acarreo.deposito.telefono,
                    dep.acarreo.deposito.horarios,
                    ubicacionDeposito);
            }

            /* Se crea el objeto infraccion para despues meter en las tarjetitas */
            infracciones[i] = new Infraccion(
                data.infracciones[i].id, data.infracciones[i].patente,
                parseFecha(new Date(data.infracciones[i].fechaHoraRegistro)),
                data.infracciones[i].fechaHoraActualizacion,
                data.infracciones[i].direccionRegistrada,
                tInfraccion.tipo.descripcion,
                data.infracciones[i].montoAPagar,
                data.infracciones[i].existeAcarreo,
                deposito);
        }

        /* Se crean e imprimen en el html tarjetitas por cada infracción */
        function addCards(infracciones) {
            var htmlCode = "<div class='flex infracs'>";

            infracciones.forEach(infraccion => {

                /* Si existe acarreo, muestra los datos del deposito y un boton para ver al 
                deposito en el mapa */
                if (infraccion.existeAcarreo == true) {

                    localStorage.setItem("UBICACIONDEPOSITOLAT", infraccion.deposito.ubicacion.lat);
                    localStorage.setItem("UBICACIONDEPOSITOLON", infraccion.deposito.ubicacion.lon);
					localStorage.setItem("DIRECCIONDEPOSITO", infraccion.deposito.direccion);

                    htmlCode +=
                        "<script>alert('Su vehiculo ha sido acarreado')</script>" +
                        "<div class='infrac'><div class=''>" +
                        "<h4><b>Infraccion #" + infraccion.id + "</b></h4>" +
                        "<p><b>Registrada el dia </b>" + infraccion.fechaHoraRegistro + "</p>" +
                        "<p><b>En la direccion </b>" + infraccion.direccionRegistrada + "</p>" +
                        "<p><b>Del tipo </b>" + infraccion.tipoInfraccion + "</p>" +
                        "<p><b>Monto a pagar </b>" + infraccion.montoAPagar + "</p>" +
                        "<p>Su vehiculo fue acarreado al deposito '" + infraccion.deposito.nombre + "'</p>" +
                        "<p>En la direccion " + infraccion.deposito.direccion + "</p>" +
                        "<p>Con horario de atencion de " + infraccion.deposito.horarios + "</p>" +
                        "<p>Y numero de telefono " + infraccion.deposito.telefono + "</p>" +
                        "<p>Para consultar la ubicacion del deposito haga clic debajo</p>" +

                        "<div class='caja_1'><div id='mapDepositoid'></div></div>" +
                        "<input class='btn full' id='verComercio5' type='button' value='Ver ubicación en el mapa' onclick='printLocation();' />" +
                        "</div></div>";

                    /* Si no existe acarreo, solo muestra los datos de la infraccion */
                } else {
                    htmlCode += "<div class='infrac'><div class=''>" +
                        "<h4><b>Infraccion #" + infraccion.id + "</b></h4>" +
                        "<p><b>Registrada el dia </b>" + infraccion.fechaHoraRegistro + "<br></p>" +
                        "<p><b>En la direccion </b>" + infraccion.direccionRegistrada + "<br></p>" +
                        "<p><b>Del tipo </b>" + infraccion.tipoInfraccion + "<br></p>" +
                        "<p><b>Monto a pagar </b>" + infraccion.montoAPagar + "<br></p>" +
                        "<p>Su vehiculo no ha sido remolcado</p></div>";
                }
            });

            htmlCode += "</div>";

            infraccionesLabel.innerHTML += htmlCode;
        }
        addCards(infracciones);
    }
}

fetchData(patente);